package br.com.fgp.arquitetura.software.java;

import br.com.fgp.arquitetura.software.java.rest.CidadeResource;
import br.com.fgp.arquitetura.software.java.rest.EventoResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.Set;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */
@ApplicationPath("rest")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(CidadeResource.class);
        resources.add(EventoResource.class);
    }
}
