package br.com.fgp.arquitetura.software.java.rest;

import br.com.fgp.arquitetura.software.java.model.Cidade;
import br.com.fgp.arquitetura.software.java.persistence.CidadePersist;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("cidade")
public class CidadeResource {

    @Inject
    private CidadePersist cidadePersist;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Cidade> getJson() {
        return cidadePersist.listar();
    }
}
