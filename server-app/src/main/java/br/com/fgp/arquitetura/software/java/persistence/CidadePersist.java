package br.com.fgp.arquitetura.software.java.persistence;

import br.com.fgp.arquitetura.software.java.model.Cidade;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Named
public class CidadePersist {

    @PersistenceContext
    private EntityManager entityManager;

    public void novaCidade(Cidade cidade) {
        entityManager.persist(cidade);
    }

    public List<Cidade> listar() {
        return entityManager.createNamedQuery("Cidade.getAll", Cidade.class).getResultList();
    }
}
