package br.com.fgp.arquitetura.software.java.queue;

import br.com.fgp.arquitetura.software.java.model.Cidade;
import br.com.fgp.arquitetura.software.java.persistence.CidadePersist;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

@MessageDriven(mappedName = "queue/PersistCidadeQueue",
        activationConfig = {
                @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
        }
)
public class CidadeMessageConsumer implements MessageListener {

    @Inject
    private CidadePersist cidadePersist;

    @Override
    public void onMessage(Message message) {
        try {
            TextMessage textMessage = (TextMessage) message;

            JsonReader jsonReader = Json.createReader(new StringReader(textMessage.getText()));
            JsonObject jsonObject = jsonReader.readObject();

            cidadePersist.novaCidade(new Cidade(jsonObject));
        } catch (JMSException ex) {
            Logger.getLogger(CidadeMessageConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
