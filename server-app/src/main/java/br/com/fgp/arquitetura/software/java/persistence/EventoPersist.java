package br.com.fgp.arquitetura.software.java.persistence;

import br.com.fgp.arquitetura.software.java.model.Evento;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */
@Named
public class EventoPersist {

    @PersistenceContext
    private EntityManager entityManager;

    public void novoEvento(Evento evento) {
        entityManager.persist(evento);
    }

    public List<Evento> listar() {
        return entityManager.createNamedQuery("Evento.getAll", Evento.class).getResultList();
    }

}
