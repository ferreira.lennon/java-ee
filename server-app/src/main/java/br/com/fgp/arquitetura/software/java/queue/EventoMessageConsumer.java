package br.com.fgp.arquitetura.software.java.queue;

import br.com.fgp.arquitetura.software.java.model.Evento;
import br.com.fgp.arquitetura.software.java.persistence.EventoPersist;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */
@MessageDriven(mappedName = "queue/PersistEventoQueue",
        activationConfig = {
                @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge"),
                @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
        }
)
public class EventoMessageConsumer implements MessageListener {

    @Inject
    private EventoPersist eventoPersist;

    @Override
    public void onMessage(Message message) {
        try {
            TextMessage textMessage = (TextMessage) message;

            JsonReader jsonReader = Json.createReader(new StringReader(textMessage.getText()));
            JsonObject jsonObject = jsonReader.readObject();

            eventoPersist.novoEvento(new Evento(jsonObject));
        } catch (JMSException ex) {
            Logger.getLogger(EventoMessageConsumer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
