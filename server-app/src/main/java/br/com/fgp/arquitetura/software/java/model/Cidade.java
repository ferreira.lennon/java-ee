package br.com.fgp.arquitetura.software.java.model;

import javax.json.JsonObject;
import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(name = "Cidade.getAll", query = "select c from Cidade c")
})
public class Cidade {

    @Id
    @GeneratedValue
    private long id;
    private String nome;
    private String codIbge;
    private String observacao;

    public Cidade() {
    }

    public Cidade(JsonObject jsonObject) {
        this.nome = jsonObject.getString("nome");
        this.codIbge = jsonObject.getString("codIbge");
        this.observacao = jsonObject.getString("observacao");
    }

    public Cidade(String nome, String codIbge, String observacao) {
        this.nome = nome;
        this.codIbge = codIbge;
        this.observacao = observacao;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodIbge() {
        return codIbge;
    }

    public void setCodIbge(String codIbge) {
        this.codIbge = codIbge;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
}
