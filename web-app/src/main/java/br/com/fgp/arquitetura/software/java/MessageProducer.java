package br.com.fgp.arquitetura.software.java;

import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.json.JsonObject;
import java.io.Serializable;

/**
 * @author Filipe Bojikian Rissi
 * @since 1.0
 */
@Named
@RequestScoped
public class MessageProducer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private JMSContext ctx;

    @Resource(mappedName = "queue/PersistEventoQueue")
    private Queue evento;

    @Resource(mappedName = "queue/PersistCidadeQueue")
    private Queue cidade;

    public void sendMessageCidade(JsonObject message) {
        ctx.createProducer().send(cidade, message.toString());
    }
    public void sendMessageEvento(JsonObject message) {
        ctx.createProducer().send(evento, message.toString());
    }
}
