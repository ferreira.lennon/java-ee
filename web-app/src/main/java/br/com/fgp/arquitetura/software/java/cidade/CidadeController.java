package br.com.fgp.arquitetura.software.java.cidade;

import br.com.fgp.arquitetura.software.java.MessageProducer;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.Json;
import javax.json.JsonObject;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Named
@RequestScoped
public class CidadeController implements Serializable {

    private static final long serialVersionUID = 1L;

    @Inject
    private MessageProducer messageProducer;

    @Inject
    @RestClient
    private CidadeRest cidadeRest;

    private String nome;
    private String codIbge;
    private String observacao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodIbge() {
        return codIbge;
    }

    public void setCodIbge(String codIbge) {
        this.codIbge = codIbge;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public void criarCidade() {
        JsonObject json = Json.createObjectBuilder()
                .add("nome", nome)
                .add("codIbge", codIbge)
                .add("observacao", observacao)
                .build();
        messageProducer.sendMessageCidade(json);

        addMessage("Cidade salva com sucesso");
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public List<Map<String, Object>> getCidades() {
        return cidadeRest.cidades();
    }
}
