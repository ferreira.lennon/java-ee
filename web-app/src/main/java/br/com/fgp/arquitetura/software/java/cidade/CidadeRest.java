package br.com.fgp.arquitetura.software.java.cidade;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

@Path("cidade")
@RegisterRestClient(baseUri = "http://localhost:8080/server/rest")
public interface CidadeRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    List<Map<String, Object>> cidades();
}
