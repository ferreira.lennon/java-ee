### ARQUITETURA DE SOFTWARE 1: JAVA

Explicações e orientações para o trabalho em grupo estão disponíveis na [Wiki](https://gitlab.com/aulas-fgp/arquitetura-de-software-1-java/java-ee/wikis/home)

Cada grupo deverá pegar uma atividade nas [issues](https://gitlab.com/aulas-fgp/arquitetura-de-software-1-java/java-ee/-/boards) do GitLab  
Ao pegar uma atividade mova ela para Doing e ao finalizar mova para Closed, dessa forma os grupos não pegarão atividades repetidas

Após finalizar a atividade abram um [merge request](https://gitlab.com/aulas-fgp/arquitetura-de-software-1-java/java-ee/merge_requests) e assinem para mim.

Meu e-mail de contato é: f.b.rissi@gmail.com

### OBS: TODOS OS ALUNOS DEVEM ESTAR EM UM GRUPO

* BIANCA VENTURINI TISZOLCZKI
* BRUNO THIAGO DA SILVA
* FELIPE BARBOSA GODOY FRANÇA
* GABRIEL MANICARDI TROVATO
* GEAN NARCIZO BARRAL LEME 
* GIOVANI PALEOLOGO
* GUILHERME AFONSO VIEIRA SOUTO
* LAEL SANTOS FRANCISCO 
* LENNON CESAR FERREIRA
* MAURÍCIO DE BASTIANI SILVA
* NATHALIA CARRASQUEIRA PERUZZO
* RAFAEL LUIS DE PAULI
* RODRIGO LUIZ VITTI
* VINICIUS MOSELLA NASCIMENTO
